import fsp from "fs/promises";
import { NextFunction, Request, RequestHandler, Response } from "express";
import HttpException from "../exceptions/Http.exception.js";



export const httpLogger =
    (path: string): RequestHandler =>
        async (req, res, next) => {
            await fsp.appendFile(
                path,
                "[" + +Date.now() + "] " + req.method + " " + req.path + "\n"
            );
            next();
        };

export default (fn: RequestHandler) =>
    (req: Request, res: Response, next: NextFunction) => {
        Promise.resolve(fn(req, res, next)).catch((err) => {
            if (err.status) next(err);
            else next(new HttpException(500, err.message));
        });
    };
