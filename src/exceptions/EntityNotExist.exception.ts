import HttpException from "./Http.exception.js";

class UserExistsException extends HttpException {
    constructor(msg: string) {
        super(400, msg);
    }
}

export default UserExistsException;
