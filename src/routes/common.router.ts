import express from "express";
import { not_found } from "../middleware/errors.handler.js";

const router = express.Router();

//when no routes were matched...
router.use("*", not_found);

export default router;
