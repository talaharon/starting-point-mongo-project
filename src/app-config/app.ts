import express from "express";
import morgan from "morgan";
import cors from "cors";
import common_router from "../routes/common.router.js";
import { errorResponse, logErrorMiddleware, printError } from "../middleware/errors.middlewares.js";
import { httpLogger } from "../middleware/common.middlewares.js";
class App {
    public app: express.Application;

    constructor() {
        this.app = express();
        this.configGeneral();
        this.configMiddlewares();
        this.configRoutes();
        this.configErrorHandlers();
    }

    configErrorHandlers() {
        const { ERR_PATH = "./logs/PLACEHOLDER.errors.log" } = process.env;
        this.app.use(printError);
        this.app.use(logErrorMiddleware(ERR_PATH));
        this.app.use(errorResponse);
    }

    private configGeneral() {
        this.app.use(cors());
        this.app.use(morgan("dev"));
        this.app.use(express.json());
    }

    private configRoutes() {
        // Other routes here
        this.app.use("*", common_router);
    }

    private configMiddlewares() {
        const { LOG_PATH = './logs/PLACEHOLDER.http.log.txt' } = process.env;
        this.app.use(httpLogger(LOG_PATH));
    }
}

export default new App().app;
